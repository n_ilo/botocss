package com.atlassian.botocss;

import cz.vutbr.web.css.Declaration;

final class BotocssExpansion {
    interface DeclarationProcessor {
        String parse(Declaration declaration);
    }

    private final String attributeName;
    private final DeclarationProcessor processor;

    BotocssExpansion(String attributeName, DeclarationProcessor processor) {
        this.attributeName = attributeName;
        this.processor = processor;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public DeclarationProcessor getProcessor() {
        return processor;
    }
}
