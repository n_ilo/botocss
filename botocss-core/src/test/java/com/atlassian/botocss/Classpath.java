package com.atlassian.botocss;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class Classpath {
    static String getResource(Class<?> clazz, String resourcePath) {
        InputStream resource = clazz.getResourceAsStream(resourcePath);
        if (resource == null) return null;

        try {
            return IOUtils.toString(resource, "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException("Could not load resource [" + resourcePath + "]: " + e.getMessage(), e);
        }
        finally {
            IOUtils.closeQuietly(resource);
        }
    }
}
