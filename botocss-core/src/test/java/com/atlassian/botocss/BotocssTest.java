package com.atlassian.botocss;

import org.junit.Test;

import static org.hamcrest.text.IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace;
import static org.junit.Assert.assertThat;

public class BotocssTest
{
    @Test
    public void testInjectString() throws Exception {
        String html = "<html><head><title>Hello</title></head><body><p>Hello, world!</p></body></html>";
        String css1 = "body { color: #f00; background: white; }";
        String css2 = "p { margin-top: 10px; }";
        String output = Botocss.inject(html, css1, css2);
        assertThat(output, equalToIgnoringWhiteSpace("<html>\n<head>\n<title>Hello</title>\n</head>\n" +
                "<body style=\"color: #ff0000; background: #ffffff\">\n<p style=\"margin-top: 10px\">Hello, world!</p>\n</body>\n</html>"));
    }

    @Test
    public void testParseAndInject() throws Exception {
        String html = "<html><head><title>Hello</title></head><body><p>Hello, world!</p></body></html>";
        String css1 = "body { color: #f00; background: white; }";
        String css2 = "p { margin-top: 10px; }";
        BotocssStyles styles = Botocss.parse(css1, css2);
        String output = Botocss.inject(html, styles);
        assertThat(output, equalToIgnoringWhiteSpace("<html>\n<head>\n<title>Hello</title>\n</head>\n" +
                "<body style=\"color: #ff0000; background: #ffffff\">\n<p style=\"margin-top: 10px\">Hello, world!</p>\n</body>\n</html>"));
    }

    @Test
    public void testInjectParsesHtmlAsExpected() throws Exception {
        //
        // this is testing an issue that was present in older version of Jsoup. Notably 1.3.3 that has been used in JIRA
        //
        // what it does, it place the end anchor </a> tag in the wrong spot, when parsed by Jsoup, hence breaking the
        // resultant output html
        //

        String html = "<html><head></head><body><table><tbody><tr><td><p>\n" +
                "\n" +
                "<p><span><a href=\"http://localhost:2990/jira/servicedesk/customershim/secure/attachment/10001/10001_Blah_-_Something_-_Blah.txt\" title=\"Blah_-_Something_-_Blah.txt attached to SD-62\">Blah_<del><em>Something</em></del>_Blah.txt<sup><img src=\"http://localhost:2990/jira/servicedesk/customershim/images/icons/link_attachment_7.gif\"/></sup></a></span> <em>(0.0 kB)</em></p></p><p>See request details</p></td></tr></tbody></table></body></html>";

        String css = "body { color: #f00; background: white; }";

        BotocssStyles styles = Botocss.parse(css);

        String output = Botocss.inject(html, styles);

        assertThat(output, equalToIgnoringWhiteSpace(
                "<html>\n" +
                "    <head></head>\n" +
                "    <body style=\"color: #ff0000; background: #ffffff\">\n" +
                "        <table>\n" +
                "            <tbody>\n" +
                "                <tr>\n" +
                "                    <td><p> </p><p><span><a href=\"http://localhost:2990/jira/servicedesk/customershim/secure/attachment/10001/10001_Blah_-_Something_-_Blah.txt\" title=\"Blah_-_Something_-_Blah.txt attached to SD-62\">Blah_<del><em>Something</em></del>_Blah.txt<sup><img src=\"http://localhost:2990/jira/servicedesk/customershim/images/icons/link_attachment_7.gif\"></sup></a></span> <em>(0.0 kB)</em></p><p></p><p>See request details</p></td>\n" +
                "                </tr>\n" +
                "            </tbody>\n" +
                "        </table>\n" +
                "    </body>\n" +
                "</html>"));
    }
}
