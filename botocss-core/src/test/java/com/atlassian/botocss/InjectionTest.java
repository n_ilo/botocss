package com.atlassian.botocss;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.botocss.Classpath.getResource;
import static org.hamcrest.text.IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class InjectionTest
{
    private final String fileName;

    public InjectionTest(String fileName) {
        this.fileName = fileName;
    }

    @Parameterized.Parameters(name = "{index} : {0}")
    public static List<Object[]> data() {
        Object[][] data = {
            {"simple"},
            {"margin"},
            {"confluence"},
            {"jira-issues"},
            {"selectors"},
            {"attribute-selectors"},
            {"inline-styles"},
            {"pseudo"},
            {"escaping"},
            {"units"},
            {"recommended"},
        };
        return Arrays.asList(data);
    }

    @Test
    public void testStyles() throws Exception {
        String html = getResource(getClass(), fileName + ".html");
        String css = getResource(getClass(), fileName + ".css");
        String output = css == null ? Botocss.inject(html) : Botocss.inject(html, css);
        final String expected = getResource(getClass(), fileName + "-output.html");
        assertThat(fileName, output, equalToIgnoringWhiteSpace(expected));
    }
}
